Obsah a instrukce k workshopu LaTeX pro začátečníky
===================================================

Stručný obsah
-------------

- sazba teoretického textu (nadpisy, odstavce, zvýraznění, matematika, definice, věta, důkaz, pseudokód, zdroják), citace, obrázky, popisky, křížové odkazy
- základní filozofie LaTeXu, výběr documentclass, parametrizace
- čeština, kódování
- instalace, backendy, ovládání z CLI (latexmk, arara), editory
- kde hledat balíčky (ctan) a dokumentaci
- verzování, CI

Podrobný obsah a průběh
-----------------------

- Představení cloud editorů: https://latexbase.com/, https://papeeria.com, https://www.overleaf.com/; ukázku uděláme v Overleaf. (5 minut)
- Otevřeme nový prázdný projekt. Vysvětlíme vše uvnitř. 
  - Doplníme dva odstavce. 
  - Přidáme podporu češtiny (babel) i kódování (inputenc, fontenc). 
  - Přidáme další nadpisy (více úrovní, hvězdička). 
  - Přidáme příkaz na obsah, přidat parametr nadpisu pro obsah. 
  - Vysvětlíme, že vzhled závisí na třídě dokumentů (ukážeme article vs. beamer). 
  - Uděláme zvýraznění (emph), a to i vnořené. Na tomhle vysvětlit WYSIWYM. (20 minut)
- Přidáme matematický výraz. (Složitost algoritmu bubble sort je $\mathcal{O}(n^2)$, kde $n \in \mathbb{N}$.) Přidáme odstavcovou matematiku (logaritmus, suma, zlomek, řecké písmo, dolní celá část, dolní i horní index). (10 minut)
  - Pro další fonty a možnosti v matematice doplníme balíček amsfonts. Zmíníme amsmath. (5 minut)
- Chci balíček amsthm pro definice, věty, důkazy. Jdu na CTAN a na dokumentaci. Vložím větu a důkaz s nadpisem a s propojeným číslováním. Použiju křížový odkaz. Nastavím theoremstyle na remark. (10 minut)
  - Úkol pro účastníky: přidejte možnost definice tak, aby obsah byl normálním písmem (ne kurzivou). Vložte takovou definici. (10 minut)
- Vložíme obrázek s popiskem. Z textu se na něj odkážeme. (5 minut)
- Přidáme balíček hyperref. (1 minuta)
- přidáme pseudokód (CTAN, algorithmicx) (20 minut)
\usepackage{algpseudocode,algorithm}

\begin{algorithm}
    \caption{Euclid’s algorithm}\label{euclid}
    \begin{algorithmic}
        \Procedure{Euclid}{$a,b$}\Comment{The g.c.d. of a and b}
            \State $r\gets a\bmod b$
            \While{$r\not=0$}\Comment{We have the answer if r is 0}
                \State $a\gets b$
                \State $b\gets r$
                \State $r\gets a\bmod b$
            \EndWhile\label{euclidendwhile}
            \State \textbf{return} $b$\Comment{The gcd is b}
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

- počeštění (\floatname{algorithm}{Algoritmus})
- WYSIWYM \newcommand{\function}[1]{\textsc{#1}}
- minted: hlavní fce v C a mintinline (5 minut)
- najít "The complexity of theorem-proving procedures" a udělat bibtex export.
- vložit bibliographystyle a biliography
- udělat větu s citací "Všechny problémy z třídy P lze polynomiální redukcí převést na problém splnitelnosti booleovských formulí" (10 minut)
- stáhnout projekt (2 minuty)
- Povíme něco o instalaci (TeXLive, MacTeX apod.). Backendy (tex, pdftex, xetex, luatex). Překlad z příkazové řádky, čtení a chápání výpisu. Nasimulujeme nějaké chyby. (10 minut)
- Ukážeme latexmk. (5 minut)
- IDE. Kile, TeXMaker. (10 minut)
- git a CI. Co (ne)verzovat. Gitlab CI. (10 minut)
